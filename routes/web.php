<?php

use App\Http\Controllers\MedicamentController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [MedicamentController::class, 'index'])->name('home');
Route::get('/dashbord', [UserController::class, 'dashbord'])->name('dashbord')->middleware('auth');
Route::get('/details/{id}', [MedicamentController::class, 'view'])->name('details');
Route::get('/arrayadd', [MedicamentController::class, 'add'])->name('arrayadd');
Route::post('/record', [MedicamentController::class, 'store'])->name('record');
Route::get('/register', [UserController::class, 'register'])->name('register');
Route::get('/login', [UserController::class, 'login'])->name('login');
Route::get('/addMedocForm', [MedicamentController::class, 'addForm'])->name('addMedocForm');

// Inscription

Route::post('/user', [UserController::class, 'inscription'])->name('inscription');
Route::post('/connexion', [UserController::class, 'connexion'])->name('connexion');
Route::post('/inscription', [UserController::class, 'logout'])->name('logout');


// CRUD medicament

Route::get('/medicament', [MedicamentController::class, 'allMedicament'])->name('medicament')->middleware('auth');
Route::post('/medocAdd', [MedicamentController::class, 'addMedicament'])->name('addMedoc')->middleware('auth');
Route::get('/medocDetail/{id}', [MedicamentController::class, 'showMedicament'])->name('medocDetails')->middleware('auth');
Route::put('/update/{id}', [MedicamentController::class, 'update'])->name('update')->middleware('auth');
Route::get('/delete/{id}', [MedicamentController::class, 'destroy'])->name('destroy')->middleware('auth');
