<!-- Start service -->
<section class="services section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2>Les médicaments disponible dans le tableau $medicaments</h2>
                    <img src="img/section-img.png" alt="#">
                    <p>La liste correspond aux objets qui se trouvent dans le tableau $medicaments.</p>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($medicaments as $medicament)
            <div class="col-lg-4 col-md-6 col-12">

                <div class="single-service">
                    <i class="icofont icofont-blood"></i>
                    <h4><a href="{{ route('details', $medicament['id']) }}">{{ $medicament['nom'] }} <span>({{ $medicament['prix'] }})</span></a></h4>
                    <p>Ceci est un médicament du tableau avec comme <strong>id={{$medicament['id']}}</strong> et le Prix est de <strong>{{ $medicament['prix'] }}</strong>. </p>
                </div>

            </div>
            {{-- <div class="col-lg-4 col-md-6 col-12">

                <div class="single-service">
                    <i class="icofont icofont-tooth"></i>
                    <h4><a href="service-details.html">Teeth Whitening</a></h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus dictum eros ut imperdiet. </p>
                </div>

            </div>
            <div class="col-lg-4 col-md-6 col-12">

                <div class="single-service">
                    <i class="icofont icofont-heart-alt"></i>
                    <h4><a href="service-details.html">Heart Surgery</a></h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus dictum eros ut imperdiet. </p>
                </div>

            </div>
            <div class="col-lg-4 col-md-6 col-12">

                <div class="single-service">
                    <i class="icofont icofont-listening"></i>
                    <h4><a href="service-details.html">Ear Treatment</a></h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus dictum eros ut imperdiet. </p>
                </div>

            </div>
            <div class="col-lg-4 col-md-6 col-12">

                <div class="single-service">
                    <i class="icofont icofont-eye-alt"></i>
                    <h4><a href="service-details.html">Vision Problems</a></h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus dictum eros ut imperdiet. </p>
                </div>

            </div>
            <div class="col-lg-4 col-md-6 col-12">

                <div class="single-service">
                    <i class="icofont icofont-blood"></i>
                    <h4><a href="service-details.html">Blood Transfusion</a></h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus dictum eros ut imperdiet. </p>
                </div>

            </div> --}}
            @endforeach
        </div>
    </div>
</section>
<!--/ End service -->
