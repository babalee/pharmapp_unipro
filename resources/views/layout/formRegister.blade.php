<div class="container p-3">
    <div class="mt-4 mb-4 row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="demo-spacing-0">
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <div class="alert-body">
                        Cette partie nécessite des interactions avec la base donnée. <br>Assurez vous de faire les
                        migrations avant de démarrer.
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            <div class="card">
                    <div class="card-header">
                        <h4 class="text-center card-title">Inscription</h4>
                    </div>
                    <div class="card-body">
                        <form class="needs-validation" novalidate="" method="POST" action="{{ route('inscription') }}">
                            @csrf
                            <div class="form-group">
                                <label class="form-label" for="prenom">Prénom</label>

                                <input type="text" id="prenom" value="{{ old('prenom') }}" class="form-control @error('prenom') is-invalid @enderror" placeholder="Ex: Baba" name="prenom" aria-describedby="basic-addon-prenom" required="">

                                <div class="invalid-feedback">@error('prenom') <span class="error"><p style="color:red">{{ $message }}</p></span> @enderror</div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="basic-addon-name">Nom</label>

                                <input type="text" id="basic-addon-name" value="{{ old('nom') }}" class="form-control @error('nom') is-invalid @enderror" placeholder="Ex: LY" name="nom" aria-describedby="basic-addon-name" required="">

                                <div class="invalid-feedback">@error('nom') <span class="error"><p style="color:red">{{ $message }}</p></span> @enderror</div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="basic-default-email1">Email</label>
                                <input type="email" id="basic-default-email1" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" placeholder="john.doe@email.com" name="email" required="">

                                <div class="invalid-feedback">@error('email') <span class="error"><p style="color:red">{{ $message }}</p></span> @enderror</div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="password">Mot de passe</label>

                                <input type="password" id="password" value="{{ old('password') }}" class="form-control @error('password') is-invalid @enderror" placeholder="Mot de passe" name="password" aria-describedby="password" required="">

                                <div class="invalid-feedback">@error('password') <span class="error"><p style="color:red">{{ $message }}</p></span> @enderror</div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="basic-addon-password_confirmation">Confirmer mot de passe</label>

                                <input type="password" id="basic-addon-password_confirm" value="{{ old('password_confirmation') }}" class="form-control @error('password_confirm') is-invalid @enderror" placeholder="Confirmer votre mot de passe" name="password_confirmation" required="">

                                <div class="invalid-feedback">@error('password_confirmation') <span class="error"><p style="color:red">{{ $message }}</p></span> @enderror</div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary waves-effect waves-float waves-light">S'inscrire</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>
