<div class="container p-3">
    <div class="mt-4 mb-4 row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="demo-spacing-0">
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <div class="alert-body">
                        Cette partie nécessite des interactions avec la base donnée. <br>Assurez vous de faire les
                        migrations avant de démarrer.
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            @if(session()->get('success'))
                <div class="demo-spacing-0">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <div class="alert-body">
                            {{ session()->get('success') }}.
                        </div>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                </div>
            @endif
            <div class="card">
                    <div class="card-header">
                        <h4 class="text-center card-title">Inscription</h4>
                    </div>
                    <div class="card-body">
                        <form class="needs-validation" novalidate=""method="POST" action="{{ route('connexion') }}">
                            @csrf
                            <div class="form-group">
                                <label class="form-label" for="basic-default-email1">Email</label>
                                <input type="email" id="basic-default-email1" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" placeholder="john.doe@email.com" name="email" required="">

                                <div class="invalid-feedback">@error('email') <span class="error"><p style="color:red">{{ $message }}</p></span> @enderror</div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="password">Mot de passe</label>

                                <input type="password" id="password" value="{{ old('password') }}" class="form-control @error('password') is-invalid @enderror" placeholder="Mot de passe" name="password" aria-describedby="password" required="">

                                <div class="invalid-feedback">@error('password') <span class="error"><p style="color:red">{{ $message }}</p></span> @enderror</div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="">Pas encore de compte ? <a href="{{ route('register') }}" class="text-primary">S'inscrire</a></label>

                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary waves-effect waves-float waves-light">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>
