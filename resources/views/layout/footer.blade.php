
    <!-- Start Newsletter Area -->
    <section class="newsletter section">
        <div class="container">
            <div class="row ">
                <div class="col-lg-6 col-12">
                    <!-- Start Newsletter Form -->
                    <div class="subscribe-text ">
                        <h6>Contacter nous !</h6>
                        {{-- <p class="">Cu qui soleat partiendo urbanitas. Eum aperiri indoctum eu,<br> homero alterum.</p> --}}
                    </div>
                    <!-- End Newsletter Form -->
                </div>
                <div class="col-lg-6 col-12">
                    <!-- Start Newsletter Form -->
                    <div class="subscribe-form ">
                        <form action="../mail/mail.php" method="get" target="_blank" class="newsletter-inner">
                            <input name="EMAIL" placeholder="Your email address" class="common-input" onfocus="this.placeholder = ''"
                                onblur="this.placeholder = 'Your email address'" required="" type="email">
                            <button class="btn">Envoyer</button>
                        </form>
                    </div>
                    <!-- End Newsletter Form -->
                </div>
            </div>
        </div>
    </section>
    <!-- /End Newsletter Area -->

    <!-- Footer Area -->
    <footer id="footer" class="footer ">
        <!-- Footer Top -->
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="single-footer">
                            <h2>A propos</h2>
                            <p>Ce TP prend en compte les tableaux et une interaction avec la base de donnée.</p>
                            <!-- Social -->
                            <ul class="social">
                                <li><a href="https://api.whatsapp.com/send?phone=221777344569"><i class="icofont-whatsapp"></i></a></li>
                                <li><a href="https://www.linkedin.com/in/mamadou-baba-ly-8224b11aa/"><i class="icofont-linkedin"></i></a></li>
                                <li><a href="#"><i class="icofont-facebook"></i></a></li>
                                <li><a href="#"><i class="icofont-google-plus"></i></a></li>
                            </ul>
                            <!-- End Social -->
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">

                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        {{-- <div class="single-footer">
                            <h2>Open Hours</h2>
                            <p>Lorem ipsum dolor sit ame consectetur adipisicing elit do eiusmod tempor incididunt.</p>
                            <ul class="time-sidual">
                                <li class="day">Monday - Fridayp <span>8.00-20.00</span></li>
                                <li class="day">Saturday <span>9.00-18.30</span></li>
                                <li class="day">Monday - Thusday <span>9.00-15.00</span></li>
                            </ul>
                        </div> --}}
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="single-footer">
                            <div class="single-footer f-link">
                                <h2>Menu</h2>
                                <div class="row">
                                    <div class="col-lg-12 col-md-6 col-12">
                                        <ul>
                                            <li><a href="{{ route('home') }}"><i class="fa fa-caret-right" aria-hidden="true"></i>Accueil</a></li>
                                            <li><a href="{{ route('login') }}"><i class="fa fa-caret-right" aria-hidden="true"></i>Connexion</a></li>
                                            <li><a href="{{ route('register') }}"><i class="fa fa-caret-right" aria-hidden="true"></i>Inscription</a></li>
                                            <li><a href="{{ route('dashbord') }}"><i class="fa fa-caret-right" aria-hidden="true"></i>Dashbord</a></li>
                                            <li><a href="{{ route('medicament') }}"><i class="fa fa-caret-right" aria-hidden="true"></i>Médicament</a></li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ End Footer Top -->
        <!-- Copyright -->
        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-12">
                        <div class="copyright-content">
                            <p>© Copyright 2021  |  LED Global Technology by <a href="https://www.wpthemesgrid.com" target="_blank">Nulka</a> </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ End Copyright -->
    </footer>
    <!--/ End Footer Area -->

    <!-- jquery Min JS -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <!-- jquery Migrate JS -->
    <script src="{{asset('js/jquery-migrate-3.0.0.js')}}"></script>
    <!-- jquery Ui JS -->
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <!-- Easing JS -->
    <script src="{{asset('js/easing.js')}}"></script>
    <!-- Color JS -->
    <script src="{{asset('js/colors.js')}}"></script>
    <!-- Popper JS -->
    <script src="{{asset('js/popper.min.js')}}"></script>
    <!-- Bootstrap Datepicker JS -->
    <script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
    <!-- Jquery Nav JS -->
    <script src="{{asset('js/jquery.nav.js')}}"></script>
    <!-- Slicknav JS -->
    <script src="{{asset('js/slicknav.min.js')}}"></script>
    <!-- ScrollUp JS -->
    <script src="{{asset('js/jquery.scrollUp.min.js')}}"></script>
    <!-- Niceselect JS -->
    <script src="{{asset('js/niceselect.js')}}"></script>
    <!-- Tilt Jquery JS -->
    <script src="{{asset('js/tilt.jquery.min.js')}}"></script>
    <!-- Owl Carousel JS -->
    <script src="{{asset('js/owl-carousel.js')}}"></script>
    <!-- counterup JS -->
    <script src="{{asset('js/jquery.counterup.min.js')}}"></script>
    <!-- Steller JS -->
    <script src="{{asset('js/steller.js')}}"></script>
    <!-- Wow JS -->
    <script src="{{asset('js/wow.min.js')}}"></script>
    <!-- Magnific Popup JS -->
    <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
    <!-- Counter Up CDN JS -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- Main JS -->
    <script src="{{asset('js/main.js')}}"></script>
