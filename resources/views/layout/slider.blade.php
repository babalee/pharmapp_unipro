

<!-- Slider Area -->
<section class="mb-4 slider">
    <div class="hero-slider">
        <!-- Start Single Slider -->
        <div class="single-slider" style="background-image:url('img/slider2.jpg')">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="text">
                            <h1>PharmApp, en construction, contient qu’un seul médicament
                                <span>{{ $medicament1['nom'] }}</span> coûtant
                                <span>{{ $medicament1['prix'] }}</span> F CFA.
                            </h1>
                            <p>Ces données provienent du variable $medicament1. </p>
                            <div class="button">
                                {{-- <a href="#" class="btn">Get Appointment</a>
                                <a href="#" class="btn primary">Learn More</a> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Single Slider -->
        <!-- Start Single Slider -->
        <div class="single-slider" style="background-image:url('img/slider.jpg')">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="text">
                            <h1>PharmApp, en construction, contient qu’un seul médicament
                                <span>{{ $medicament1['nom'] }}</span> coûtant
                                <span>{{ $medicament1['prix'] }}</span> F CFA.
                            </h1>
                            <p>Ces données provienent du variable $medicament1.</p>
                            <div class="button">
                               {{--  <a href="#" class="btn">Get Appointment</a>
                                <a href="#" class="btn primary">About Us</a> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Start End Slider -->
        <!-- Start Single Slider -->
        <div class="single-slider" style="background-image:url('img/slider3.jpg')">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="text">
                            <h1>PharmApp, en construction, contient qu’un seul médicament
                                <span>{{ $medicament1['nom'] }}</span> coûtant
                                <span>{{ $medicament1['prix'] }}</span> F CFA.
                            </h1>
                            <p>Ces données provienent du variable $medicament1. </p>
                            <div class="button">
                                {{-- <a href="#" class="btn">Get Appointment</a>
                                <a href="#" class="btn primary">Conatct Now</a> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Single Slider -->
    </div>
</section>
<!--/ End Slider Area -->

