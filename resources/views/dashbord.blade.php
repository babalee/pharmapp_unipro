<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('layout.header')
    </head>
    <body>
        @include('layout.nav')
        <div class="container p-4">
            <div class="mt-4 row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">{{ __('Dashboard') }}</div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            {{ __('Bienvenue dans pharmApp ') }} <strong>{{ $user->prenom }} {{ $user->nom }}</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layout.footer')
    </body>
</html>
