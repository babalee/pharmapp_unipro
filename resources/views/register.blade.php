<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('layout.header')
    </head>
    <body>
        @include('layout.nav')
        @include('layout.formRegister')
        @include('layout.footer')
    </body>
</html>
