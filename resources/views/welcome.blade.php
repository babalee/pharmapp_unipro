<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('layout.header')
    </head>
    <body>
        @include('layout.nav')
        @include('layout.slider')
        @include('layout.schedule')
        @include('layout.medocArray')
        @include('layout.contact')
        @include('layout.addMedoc')
        @include('layout.footer')
    </body>
</html>
