<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('layout.header')
    </head>
    <body>
        @include('layout.nav')
            <div class="container p-4">
                <div class="mt-4 row">
                    <div class="col-md-12">

                    <div class="card">
                        <div class="card-header">PharmApp</div>
                        <div class="card-body d-flex justify-content-between">
                            <h4 class="card-title">Liste des médicaments dans la base de données</h4>

                            <a href="{{ route('addMedocForm') }}" class="text-white btn btn-outline-primary waves-effect">Ajouter</a>
                        </div>
                    </div>
                    @if(session()->get('destroy'))
                    <div class="mt-4 demo-spacing-0">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <div class="alert-body">
                                {{ session()->get('destroy') }}.
                            </div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                    @endif
                    <table class="table mt-4 table-hover table-bordered">
                    <thead class="table-primary">
                        <tr>
                        <th scope="col">Nom</th>
                        <th scope="col">Prix</th>
                        <th scope="col" class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($medicaments)>0)
                            @foreach($medicaments as $medicament)
                                <tr>
                                    <td>{{ $medicament->nom }}</td>
                                    <td>{{ $medicament->prix }}</td>
                                    <td class="justify-content-center">

                                        <a href="{{ route('medocDetails', $medicament->id) }}" class="mr-2 text-white btn btn-outline-info"><i class="mr-1 fa fa-edit"></i>Edit</a>

                                        <a href="{{ route('destroy', $medicament->id) }}" class="text-white btn btn-outline-danger"><i class="mr-1 fa fa-edit"></i>Delete</a>

                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td>Aucun médicament n'est encore enregistré</td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endif

                    </tbody>
                    </table>
                    </div>
                </div>
            </div>
        @include('layout.footerHome')
    </body>
</html>
