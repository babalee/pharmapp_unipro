<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('layout.header')
    </head>
    <body>
        @include('layout.nav')


        <div class="container p-4">
            <div class="mt-4 row">
                <div class="col-md-12">
                    @if(session()->get('update'))
                        <div class="demo-spacing-0">
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <div class="alert-body">
                                    {{ session()->get('update') }}.
                                </div>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        </div>
                        @endif
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Modification médicament</h4>
                        </div>

                        <div class="card-body">
                            <form class="form form-vertical" method="POST" action="{{ route('update', $medicament->id) }}">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="first-name-vertical">Nom</label>
                                            <input type="text" id="first-name-vertical" class="form-control @error('nom') is-invalid @enderror" value="{{ $medicament->nom }}" name="nom" placeholder="ex: Paracétamol">
                                            <div class="invalid-feedback">@error('nom') <span class="error"><p style="color:red">{{ $message }}</p></span> @enderror</div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="email-id-vertical">Prix</label>
                                            <input type="number" id="email-id-vertical" class="form-control @error('prix') is-invalid @enderror" value="{{ $medicament->prix }}" name="prix" placeholder="ex: 3750">
                                            <div class="invalid-feedback">@error('prix') <span class="error"><p style="color:red">{{ $message }}</p></span> @enderror</div>
                                        </div>
                                    </div>
                                    <div class="col-12 d-flex">
                                        <button type="submit" class="mr-1 btn btn-primary waves-effect waves-float waves-light">Modifier</button>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
