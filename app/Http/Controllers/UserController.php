<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller

{
    public function __construct()
    {
        $this->middleware('auth')->except('login', 'register', 'inscription', 'connexion');
    }

    public function index()
    {
        return view('login');
    }

    public function dashbord()
    {
        return view('dashbord');
    }

    public function inscription(Request $request)
    {
        {
            $this->validate($request, [
                'prenom'=>'required',
                'nom'=>'required',
                'password'=>'required|string|min:8|confirmed',
                'email'=>'required|unique:users',

            ]);


            $mdp=$request->input('password');

            $user = new User();
            $user->prenom=$request->input('prenom');
            $user->nom=$request->input('nom');
            $user->email=$request->input('email');
            $user->password= Hash::make($mdp);

            //dd($user);

            $user->save();
            return view('/dashbord', compact('user')) ->with('success', "Inscription réussie avec succès !");
        }
    }

    public function connexion(Request $request)
    {
        $this->validate($request, [

            'password'=>'required',
            'email'=>'required',

        ]);

        $userLog = $request->only('email', 'password');

        if(Auth::attempt($userLog)){
            $user = User::find(Auth::user()->id);
            return view('/dashbord', compact('user')) ->with('success', "connexion réussi");
        }

        return view('/login') ->with('success', "Email ou mot de passe incorrect !");
    }

    public function logout(Request $request){
        //$request->session()->flush();
        Auth::logout();

        return redirect('login');
    }



    public function login()
    {
        return view('login');
    }

    public function register()
    {
        return view('register');
    }
}
