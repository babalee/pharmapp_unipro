<?php

namespace App\Http\Controllers;

use App\Models\Medicament;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class MedicamentController extends Controller
{

    public $medicament = ['id'=>1, 'nom'=>'Efferalgan', 'prix'=>1187];
    public $medicaments = [
        ['id'=>2, 'nom'=>'Paracétamol', 'prix'=>2350],
        ['id'=>3, 'nom'=>'Doliprane', 'prix'=>4275],
        ['id'=>4, 'nom'=>'Cétamine', 'prix'=>3760],
        ['id'=>5, 'nom'=>'Ubiprofene', 'prix'=>800],
    ];


    public function index()
    {
        $medicament1 = $this->medicament;
        $medicaments = $this->medicaments;
        return view('welcome', compact('medicament1', 'medicaments'));
    }


    public function view($medoc)
    {

        session(['id' => $medoc]);
        foreach($this->medicaments as $med){

            if($med['id']=$medoc){
                $detail = Arr::last($this->medicaments, function($value, $key){
                    return $value['id'] == session('id');
                });
                break;
            }
        }


        return view('view', compact('detail'));
    }


    public function add(Request $request)
    {
        return view('add');
    }


    public function store(Request $request)
    {
        $request->validate([
            "nom"=>"required",
            "prix"=>"required"
        ]);

        $id = count($this->medicaments) + 2;
        $newMedoc = ['id'=> $id,'nom'=>$request->get('nom'), 'prix'=>$request->get('prix')];


        $a = Arr::prepend($this->medicaments, $newMedoc);

        return response()->json($a, 200);
    }


    public function edit(Medicament $medicament)
    {
        //
    }

    public function allMedicament(){
        $medicaments = Medicament::All();
        return view('crudLayout.listMedoc', compact('medicaments'));
    }

    public function addMedicament(Request $request){
        $this->validate($request, [
            'nom'=>'required',
            'prix'=>'required|numeric',

        ]);

        $medicament = new Medicament();
        $medicament->nom=$request->input('nom');
        $medicament->prix=$request->input('prix');

        $medicament->save();

        session()->flash('ajouter', 'Médicament créer avec succès !');

        $medicaments = Medicament::All();
        return view('crudLayout.listMedoc', compact('medicaments'));
    }

    public function showMedicament($id){
        $medicament = Medicament::find($id);
        return view('crudLayout.editMedoc', compact('medicament'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nom'=>'required',
            'prix'=>'required|numeric',

        ]);

        $medicament = Medicament::find($id);
        $medicament->nom=$request->input('nom');
        $medicament->prix=$request->input('prix');

        $medicament->save();
        session()->flash('update', 'Médicament modifier avec succès.');

        return view('crudLayout.editMedoc', compact('medicament'));
    }

    public function destroy($id)
    {
        //dd($id);
        $medicament = Medicament::where('id',$id)->delete();

        session()->flash('destroy', 'Médicament supprimer avec succès.');

        $medicaments = Medicament::All();
        return view('crudLayout.listMedoc', compact('medicaments'));
    }

    public function addForm(){
        return view('crudLayout.addMedoc');
    }
}
